def StopIteration():
    my_list = [1, 2, 3]
    my_iter = iter(my_list)
    while True:
        print(next(my_iterator))

def ZeroDivisionError():
    x = 10
    y = 0
    return x/y

def AssertionError():
    x = 10
    y = 5
    assert x < y

def ImportError():
    import sigit

def KeyError():
    my_dict = {"itay":1,"sigit":2}
    print(my_dict["gavar"])

def SyntaxError():
    if number
        print(number)

def IndentationError():
    if number:
    print(number)

def TypeError():
    x = 10
    y = "5"
    return x+y

