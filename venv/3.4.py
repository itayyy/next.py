class UsernameContainsIllegalCharacter(Exception):
    def __init__(self,arg,charcter,index):
        self.arg = arg
        self.charcter = charcter
        self.index = index
    def __str__(self):
        return f"Username contains illegal character '{self.charcter}' at index {self.index}"



class UsernameTooShort(Exception):
    def __init__(self,arg):
        self.arg = arg
    def __str__(self):
        return "the username is tooshort"

class UsernameTooLong(Exception):
    def __init__(self,arg):
        self.arg = arg
    def __str__(self):
        return "the username is toolong"
class PasswordMissingCharacter(Exception):
    def __init__(self,arg):
        self.arg = arg
    def __str__(self):
        return "the password missing charcter"
class PasswordTooShort(Exception):
    def __init__(self,arg):
        self.arg = arg
    def __str__(self):
        return "the password tooshort"
class PasswordTooLong(Exception):
    def __init__(self,arg):
        self.arg = arg
    def __str__(self):
        return "the password toolong"


class PasswordMissingCharacter(Exception):
    def __str__(self):
        return "Password is missing a character."


class PasswordMissingUppercase(PasswordMissingCharacter):
    def __str__(self):
        return "Password is missing an uppercase letter."


class PasswordMissingLowercase(PasswordMissingCharacter):
    def __str__(self):
        return "Password is missing a lowercase letter."


class PasswordMissingDigit(PasswordMissingCharacter):
    def __str__(self):
        return "Password is missing a digit."


class PasswordMissingSpecial(PasswordMissingCharacter):
    def __str__(self):
        return "Password is missing a special character."


def check_input(username, password):
    try:
        for letter in username:
            if not str(letter).isdigit() or not str(letter).isalpha() or letter!="_" :
                raise UsernameContainsIllegalCharacter(username,4)

        if len(username)<3:
            raise UsernameTooShort(username)
        if len(username)>16:
            raise UsernameTooLong(username)

        if not any(c.isupper() for c in password):
            raise PasswordMissingUppercase()
        if not any(c.islower() for c in password):
            raise PasswordMissingLowercase()
        if not any(c.isdigit() for c in password):
            raise PasswordMissingDigit()
        if not any(c in string.punctuation for c in password):
            raise PasswordMissingSpecial()

        if len(password)<8:
            raise PasswordTooShort(password)
        if len(password)>40:
            raise PasswordTooLong(password)

        print("ok")

    except UsernameContainsIllegalCharacter as e :
        print(e)
    except UsernameTooShort as e:
        print(e)
    except UsernameTooLong  as e:
        print(e)
    except PasswordMissingCharacter  as e:
        print(e)
    except PasswordTooShort  as e:
        print(e)
    except PasswordTooLong  as e:
        print(e)
def main():
    check_input("A_a1.", "12345678")
    check_input("A_1", "abcdefghijklmnop")
    check_input("A_1", "ABCDEFGHIJLKMNOP")
    check_input("A_1", "ABCDEFGhijklmnop")
    check_input("A_1", "4BCD3F6h1jk1mn0p")

main()
