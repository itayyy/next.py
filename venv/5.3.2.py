class MusicNotes:
    def __init__(self):
        self.index = 1
        self.place = 0 #run all over the notes

    def __iter__(self):
        return self


    def __next__(self):
        notes = [("La", 55), ("Si", 61.74), ("Do", 65.41), ("Re", 73.42), ("Mi", 82.41), ("Fa", 87.31), ("Sol", 98)]
        frequency = notes[self.place][1] *self.index

        if self.index == 4 :
            raise StopIteration

        if self.place>len(notes):
            self.place = 0
            self.index += 1
        else:
            self.place += 1

        return frequency

def main():
    notes_iter = iter(MusicNotes())
    for freq in notes_iter:
        print(freq)
main()