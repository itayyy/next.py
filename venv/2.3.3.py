class Octopus:
    count_animals = 0
    def __init__(self,name="octavio"):
        Octopus.count_animals+=1
        self._name = name
        self._age = 8

    def birthday(self):
        self._age+=1
    def get_age(self):
        return self._age
    def set_name(self,another_name):
        self._name = another_name
    def get_name(self):
        return self._name


def main():
    instance1 = Octopus()
    instance2 = Octopus("sigit")
    instance1.birthday()
    print(instance1.get_name())
    print(instance2.get_name())
    instance2.set_name("itay")
    print(instance2.get_name())
    print(Octopus.count_animals)
main()