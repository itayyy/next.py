def translate(sentence):
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    translate_sentence = (words.get(word, word) for word in sentence.split())
    new_sentence = " "
    for i in sentence.split():
        new_sentence += next(translate_sentence) + " "
    return new_sentence.strip()
def main():
    translated_sentence = translate("el gato esta en la casa")
    print(translated_sentence)
main()
