class Pixel():
    def __init__(self, x=0, y=0, red=0, blue=0, green=0):
        self._x = x
        self._y = y
        self._red = red
        self._blue = blue
        self._green = green

    def set_coords(self, x, y):
        self._x = x
        self._y = y
        self

    def set_greyscale(self):
        avrage = (self._red + self._blue + self._green) // 3
        self._red = avrage
        self._blue = avrage
        self._green = avrage

    def print_pixel_info(self):
        color_combo = (self._red, self._green, self._blue)
        color_names = ('red', 'green', 'blue')
        non_zero_colors = [color_names[i] for i in range(3) if color_combo[i] > 0]
        if non_zero_colors is not None and len(non_zero_colors) == 1:
            print("X: " + str(self._x) + ", Y: " + str(self._y) + ", Color: " + str(color_combo) + " " + str(
                non_zero_colors[0]))
        else:
            print("X: " + str(self._x) + ", Y: " + str(self._y) + ", Color: " + str(color_combo))


def main():
    p = pixel(5, 6, 250)
    p.print_pixel_info()
    p.set_greyscale()
    p.print_pixel_info()
