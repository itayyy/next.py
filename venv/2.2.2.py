class Octopus:
    def __init__(self,name,age):
        self.name = name
        self.age = age

    def birthday(self):
        self.age+=1
    def get_age(self):
        return self.age

def main():
    instance1 = Octopus("octavio",4)
    instance2 = Octopus("sigit",15)
    instance1.birthday()
    print(instance1.get_age())
    print(instance2.get_age())
main()