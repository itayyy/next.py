import functools
def add(a, b):
    return int(a) + int(b)

def sum_of_digits(number):
    return functools.reduce(add, str(number))

if __name__ == '__main__':

    print(sum_of_digits(104))
