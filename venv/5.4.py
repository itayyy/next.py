class IDIterator:
    def __init__(self,id):
        self._id = id
    def __iter__(self):
        return self
    def __next__(self):
        while self._id <= 999999999:
            if check_id_valid(self._id):
                valid_id = self._id
                self._id += 1
                return valid_id
            else:
                self._id += 1
        raise StopIteration


def check_id_valid(id_number):
    numbers_list = []
    num = id_number
    numbers_list.append(0)
    for i in range(1, 10):
        numbers_list.append(num % 10)
        num = num // 10
    total_sum = 0

    for i in range(1,len(numbers_list)):

        mul = 0
        if i % 2 == 0:
            mul = numbers_list[i] * 2
        else:
            mul = numbers_list[i]
        if mul > 9:
            mul = (mul % 10) + (mul // 10)
        total_sum += mul
    return total_sum % 10 == 0

def id_generator(id):
    while id <= 999999999:
        if check_id_valid(id):
            yield id
        id = id+1


def main():
    id_input = int(input("enter id :"))
    str_input = input("Generator or Iterator? (gen/it)?")
    if str_input=="gen":
        check_genrator = id_generator(id_input)
        for i in range(10):
            print(next(check_genrator))
    elif str_input=="it":
        check_iterator = IDIterator(id_input)
        for i in range(10):
            print(next(check_iterator))
    else:
        print("Illegal input")


main()