
def sum_length(file):
    file_open = open(file,'r')
    words = file_open.read().split()
    return sum(len(word) for word in words)
if __name__ == '__main__':
    file = 'C:\\Users\\user-pc\\Downloads\\file1.txt'
    print(sum_length(file))