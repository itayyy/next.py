import tkinter as tk
from PIL import Image, ImageTk


def show_image():
    # Open the image
    image = Image.open("C:\\Users\\user-pc\\Downloads\\nextpy.png")  # Replace "image.jpg" with the path to your image file

    # Create a new window
    image_window = tk.Toplevel()
    image_window.title("sigit")

    # Convert the image to a Tkinter-compatible format
    photo = ImageTk.PhotoImage(image)

    # Create a label and display the image
    label = tk.Label(image_window, image=photo)
    label.pack()

    # Update the image window to ensure the image is displayed
    image_window.update()


# Create the main window
window = tk.Tk()
window.title("Image Question")
window.
# Create a label with the question
question_label = tk.Label(window, text="what your favorite video:")
question_label.pack()

# Create a button to open the image
button = tk.Button(window, text="click here", command=show_image)
button.pack()

# Start the Tkinter event loop
window.mainloop()
