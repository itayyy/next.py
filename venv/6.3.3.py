import pyttsx3

# Create a text-to-speech engine
engine = pyttsx3.init()

# Set the speed and volume of the speech
engine.setProperty('rate', 150)
engine.setProperty('volume', 1.0)

# Define the sentence to be spoken
sentence = "First time I'm using a package in the Next.py course"

# Convert the sentence to speech and play it
engine.say(sentence)
engine.runAndWait()
