def gen_secs():
    return (second for second in range(0,60))
def gen_minutes():
    return (minute for minute in range(0,60))
def gen_hours():
    return (hour for hour in range(0,24))
def gen_time():
    for hour in gen_hours():
        for minute in gen_minutes():
            for second in gen_secs():
                yield "%02d:%02d:%02d" % (hour, minute, second)
def gen_years(start=2019):
    year = start
    while True:
        yield year
        year += 1
def gen_months():
    return (month for month in range(1,13))

def gen_days(month, leap_year=True):
    days_in_month = {
        1: 31,
        2: 29 if leap_year else 28,
        3: 31,
        4: 30,
        5: 31,
        6: 30,
        7: 31,
        8: 31,
        9: 30,
        10: 31,
        11: 30,
        12: 31
    }
    return (day for day in range(1, days_in_month[month] + 1))

def gen_date():
    for year in gen_years(2019):
        is_leap_year = False
        if year%4==0:
            is_leap_year = True
        if year%100==0:
            is_leap_year = False
        if year%400==0:
            is_leap_year = True

        for month in gen_months():
            for day in gen_days(month,is_leap_year):
                for hour in gen_hours():
                    for minute in gen_minutes():
                        for second in gen_secs():
                            yield "%02d/%02d/%02d " " %02d:%02d:%02d" % (day, month, year,hour, minute, second)





def main():
    for gt in gen_date():
        print(gt)
    i = 1
    while True:
        date = next(gen_date())
        if i % 1000000 == 0:
            print(date)
        i += 1


main()