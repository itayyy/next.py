class BigThing:
    def __init__(self,something):
        self.something = something

    def size(self):
        if isinstance(self.something,int):
            return self.something
        elif isinstance(self.something,str) or isinstance(self.something,list) or isinstance(self.something,dict):
            return len(self.something)

class BigCat(BigThing):
    def __init__(self,something,weight):
        BigThing.__init__(self,something)
        self.weight = weight

    def size(self):
        if self.weight>15 and self.weight<20:
            print("Fat")
        elif self.weight>20:
            print("Very Fat")
if __name__ == '__main__':
    my_thing = BigThing("balloon")
    print(my_thing.size())
    cutie = BigCat("mitzy", 22)
    print(cutie.size())