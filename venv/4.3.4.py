def get_fibo():
    x= 0
    y=1
    yield x
    yield y
    while True:
        x=x+y
        yield x
        y=x+y
        yield y



def main():
    fibo_gen = get_fibo()
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))

main()