def is_prime(number):
    return number > 1 and [i for i in range(2, int(number**0.5)+1) if number % i == 0] == [] #check if the list is empty

if __name__ == '__main__':
    print(is_prime(43))


