class UnderAge(Exception):

    def __init__(self,arg):
        self.arg = arg

    def __str__(self):
        return "Provided argument %s is under 18 age in %s you can get invite to the party." % self.arg,18-self.arg

    def get_arg(self):
        return self.arg


def send_invitation(name, age):
    try:
        if int(age) < 18:
            raise UnderAge(age)
        else:
            print("You should send an invite to " + name)

    except UnderAge as e:
        print(e.get_arg())

def main():
    send_invitation("itay",17)
main()